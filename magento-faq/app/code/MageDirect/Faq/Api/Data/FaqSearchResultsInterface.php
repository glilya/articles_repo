<?php

namespace MageDirect\Faq\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface FaqSearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get faqs list
     *
     * @return \MageDirect\Faq\Api\Data\FaqInterface[]
     */
    public function getItems();

    /**
     * Set faqs list
     *
     * @param \MageDirect\Faq\Api\Data\FaqInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
