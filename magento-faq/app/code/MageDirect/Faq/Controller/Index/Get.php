<?php

namespace Magedirect\Faq\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use MageDirect\Faq\Api\FaqRepositoryInterface;

class Get extends Action
{

    /**
     * @var FaqRepositoryInterface 
     */
    protected $faqRepository;

    /**
     * @param Context $context
     * @param FaqRepositoryInterface $faqRepository
     */
    public function __construct(
            Context $context, 
            FaqRepositoryInterface $faqRepository
    ) {
        $this->faqRepository = $faqRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->getResponse()->setHeader('content-type', 'text/plain');

        $faq = $this->faqRepository->getById(5);
        $this->getResponse()->appendBody(sprintf(
                "ID: %d\nTitle: %s\nContent: %s\nCreated: %s\nUpdated: %s\nIs active: %s",
                $faq->getId(),
                $faq->getTitle(), 
                $faq->getContent(),
                $faq->getCreatedAt(),
                $faq->getUpdatedAt(),
                $faq->isActive())
        );
    }

}
