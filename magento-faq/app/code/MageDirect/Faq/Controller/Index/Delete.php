<?php

namespace Magedirect\Faq\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use MageDirect\Faq\Api\FaqRepositoryInterface;

class Delete extends Action
{

    /**
     * @var FaqRepositoryInterface 
     */
    protected $faqRepository;

    /**
     * @param Context $context
     * @param FaqRepositoryInterface $faqRepository
     */
    public function __construct(
            Context $context, 
            FaqRepositoryInterface $faqRepository
    ) {
        $this->faqRepository = $faqRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $faq = $this->faqRepository->getById(1);
        $this->faqRepository->delete($faq);
        $this->faqRepository->deleteById(2);
    }

}
