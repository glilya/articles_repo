<?php

namespace Magedirect\Faq\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use MageDirect\Faq\Api\FaqRepositoryInterface;
use MageDirect\Faq\Api\Data\FaqInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\Api\SortOrder;

class Index extends Action
{

    /**
     * @var FaqRepositoryInterface 
     */
    protected $faqRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;
    
    /**
     * @var FilterBuilder
     */
    protected $filterBuilder;
    
    /**
     * @var SortOrderBuilder
     */
    protected $sortOrderBuilder;

    /**
     * @param Context $context
     * @param FaqRepositoryInterface $faqRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param SortOrderBuilder $sortOrderBuilder
     */
    public function __construct(
            Context $context, 
            FaqRepositoryInterface $faqRepository, 
            SearchCriteriaBuilder $searchCriteriaBuilder,
            FilterBuilder $filterBuilder,
            SortOrderBuilder $sortOrderBuilder
    ) {
        $this->faqRepository = $faqRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->getResponse()->setHeader('content-type', 'text/plain');

        $filters[] = $this->filterBuilder
                    ->setConditionType('like')
                    ->setField(FaqInterface::TITLE)
                    ->setValue('Question%')
                    ->create();
        $this->searchCriteriaBuilder->addFilters($filters);
        $this->searchCriteriaBuilder->addSortOrder(
                $this->sortOrderBuilder
                        ->setField(FaqInterface::FAQ_ID)
                        ->setDirection(SortOrder::SORT_DESC)
                        ->create()
        );
        $this->searchCriteriaBuilder->setPageSize(4);
        $this->searchCriteriaBuilder->setCurrentPage(1);
        $faqs = $this->faqRepository->getList($this->searchCriteriaBuilder->create());

        $this->getResponse()->appendBody(sprintf("Result count: %d\n=============\n", $faqs->getTotalCount()));
        foreach ($faqs->getItems() as $faq) {
            $this->getResponse()->appendBody(sprintf(
                "ID: %d\nTitle: %s\nContent: %s\nCreated: %s\nUpdated: %s\nIs active: %s\n=============\n",
                $faq->getId(),
                $faq->getTitle(), 
                $faq->getContent(),
                $faq->getCreatedAt(),
                $faq->getUpdatedAt(),
                $faq->isActive()));
        }
        
    }

}
