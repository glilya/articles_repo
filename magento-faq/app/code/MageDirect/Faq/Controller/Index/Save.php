<?php

namespace Magedirect\Faq\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use MageDirect\Faq\Api\Data\FaqInterfaceFactory;
use MageDirect\Faq\Api\FaqRepositoryInterface;

class Save extends Action
{

    /**
     * @var FaqInterfaceFactory
     */
    protected $faqFactory;

    /**
     * @var FaqRepositoryInterface 
     */
    protected $faqRepository;

    /**
     * @param Context $context
     * @param FaqInterfaceFactory $faqFactory
     * @param FaqRepositoryInterface $faqRepository
     */
    public function __construct(
            Context $context, 
            FaqInterfaceFactory $faqFactory, 
            FaqRepositoryInterface $faqRepository
    ) {
        $this->faqFactory = $faqFactory;
        $this->faqRepository = $faqRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $faq = $this->faqFactory->create();
        $faq->setTitle('Test title');
        $faq->setContent('Test content');
        $faq->setIsActive(false);
        $this->faqRepository->save($faq);
    }

}
